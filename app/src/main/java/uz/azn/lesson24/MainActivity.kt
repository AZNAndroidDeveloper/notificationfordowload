package uz.azn.lesson24

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.core.app.NotificationCompat


var progress = 0

class MainActivity : AppCompatActivity() {
    lateinit var notificationBuilder: Notification.Builder
    lateinit var notificationManager: NotificationManager
    val max = 100
    val min = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val button = findViewById<Button>(R.id.notification_button)
        button.setOnClickListener {
            senNotification()
        }
    }

    private fun senNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel("1200", "com.azn.Lesson24", importance)
            notificationChannel.setSound(null, null)

            // bu asosan bosilganda boshqa activityga otadi 
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://youtube.com"))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            val pendingIntent = PendingIntent.getActivity(this, 100, intent, 0)
            val bitmap = BitmapFactory.decodeResource(resources, R.drawable.selena)

            notificationBuilder = Notification.Builder(this, 1200.toString())
                    .setSmallIcon(R.drawable.ic_house)
                    .setContentTitle("Yuklanmoqda")
                    .setLargeIcon(bitmap)
                    .setContentText("$progress%")
                    .setTimeoutAfter(10000)
                    .setProgress(max, min, true)
                    .addAction(R.drawable.selena, "youtube.com", pendingIntent)
                    .setPriority(Notification.PRIORITY_DEFAULT)
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
            notificationManager.notify(0, notificationBuilder.build())


            Thread {
                while (progress < max) {
                    Thread.sleep(2000)
                    progress += 10

                    notificationBuilder.setProgress(max, progress, false)
                            .setContentTitle("Iltimos kuting")
                            .setContentText("$progress")
                    notificationManager.notify(0, notificationBuilder.build())

                }
                notificationBuilder.setProgress(0, 0, false)
                        .setContentTitle("SelenaGomes")
                        .setContentText("Yuklab olindi")
                notificationManager.notify(0, notificationBuilder.build())

            }.start()
        }
    }
}